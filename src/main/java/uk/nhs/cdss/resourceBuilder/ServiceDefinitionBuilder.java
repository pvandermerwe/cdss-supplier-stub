package uk.nhs.cdss.resourceBuilder;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Enumerations.DataType;
import org.hl7.fhir.dstu3.model.Enumerations.ResourceType;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier.IdentifierUse;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.ServiceDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.nhs.cdss.entities.DataRequirementEntity;
import uk.nhs.cdss.entities.ServiceDefinitionEntity;
import uk.nhs.cdss.repos.DataRequirementRepository;
import uk.nhs.cdss.repos.ServiceDefinitionRepository;

@Component
public class ServiceDefinitionBuilder {

	@Autowired
	private DataRequirementRepository dataRequirementRepository;

	@Autowired
	private DataRequirementBuilder dataRequirementBuilder;

	@Autowired
	private ServiceDefinitionRepository serviceDefinitionRepository;

	public ServiceDefinition createServiceDefinition(Long id) {
		ServiceDefinitionEntity entity = serviceDefinitionRepository.findOne(id);
		return createServiceDefinition(entity, true);
	}

	public ServiceDefinition createServiceDefinition(ServiceDefinitionEntity entity, Boolean resourcesNotContained) {
		ServiceDefinition serviceDefinition = new ServiceDefinition();
		serviceDefinition.setUrl("CDSS-Supplier-Stub/ServiceDefinition/" + entity.getId());
		serviceDefinition.setId(new IdType(entity.getId()));

		setDescriptiveData(entity, serviceDefinition);
		setIdentifier(entity, serviceDefinition);
		addUseContext(entity.getPartyCode(), entity.getPartyDisplay(), serviceDefinition);
		addUseContext(entity.getSkillsetCode(), entity.getSkillsetDisplay(), serviceDefinition);
		addJurisdiction(entity, serviceDefinition);
		addTopic(serviceDefinition);
		addDataRequirements(entity.getId(), serviceDefinition, resourcesNotContained);

		return serviceDefinition;
	}

	private void addDataRequirements(Long id, ServiceDefinition serviceDefinition, Boolean resourcesNotContained) {
		List<DataRequirementEntity> dataRequirements = dataRequirementRepository.findByServiceDefinitionId(id);
		
		dataRequirements.forEach(dataRequirementEntity -> serviceDefinition.addDataRequirement(
						dataRequirementBuilder.buildDataRequirement(ResourceType.QUESTIONNAIRERESPONSE.toCode(), 
								"https://www.hl7.org/fhir/questionnaireresponse.html", dataRequirementEntity, resourcesNotContained)));
		
		serviceDefinition.addTrigger().setEventData(
				dataRequirementBuilder.buildDataRequirement(DataType.TRIGGERDEFINITION.toCode(), 
						"https://www.hl7.org/fhir/triggerdefinition.html", dataRequirements.get(0), resourcesNotContained));
	}

	private void addTopic(ServiceDefinition serviceDefinition) {
		serviceDefinition.addTopic().setText("Triage").addCoding().setSystem("CDSS Supplier Stub").setVersion("1")
				.setCode("TRI").setDisplay("Triage").setUserSelected(false);
	}

	private void addJurisdiction(ServiceDefinitionEntity entity, ServiceDefinition serviceDefinition) {
		serviceDefinition.addJurisdiction().setText("England").addCoding().setSystem("CDSS Supplier Stub")
				.setVersion("1").setCode(entity.getJurisdiction()).setDisplay("England").setUserSelected(false);
	}

	private void addUseContext(String code, String display, ServiceDefinition serviceDefinition) {
		serviceDefinition.addUseContext().setCode(new Coding().setSystem("CDSS Supplier Stub").setVersion("1")
				.setCode(code).setDisplay(display).setUserSelected(false));
	}

	private void setIdentifier(ServiceDefinitionEntity entity, ServiceDefinition serviceDefinition) {
		serviceDefinition.addIdentifier().setUse(IdentifierUse.USUAL).setSystem("CDSS Supplier Stub")
				.setValue("Test Service Definition Scenario " + entity.getScenarioId())
				.setPeriod(new Period().setStart(new Date())).setAssigner(new Reference("NHS Digital"));
	}

	private void setDescriptiveData(ServiceDefinitionEntity entity, ServiceDefinition serviceDefinition) {
		serviceDefinition.setVersion("1").setName("scenario-" + entity.getScenarioId())
				.setTitle("Scenario " + entity.getScenarioId()).setStatus(entity.getStatus())
				.setExperimental(true).setDate(new Date()).setPublisher("NHS Digital")
				.setDescription(entity.getDescription()).setPurpose(entity.getPurpose()).setUsage(entity.getPurpose())
				.setApprovalDate(new Date()).setLastReviewDate(new Date())
				.setEffectivePeriod(new Period().setStart(new Date()));
	}
}
