package uk.nhs.cdss.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus;

@Entity
@Table(name = "service_definition")
public class ServiceDefinitionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "scenario_id")
	private String scenarioId;

	@Column(name = "description")
	private String description;

	@Column(name = "purpose")
	private String purpose;

	@Column(name = "party_code")
	private String partyCode;

	@Column(name = "party_display")
	private String partyDisplay;

	@Column(name = "skillset_code")
	private String skillsetCode;

	@Column(name = "skillset_display")
	private String skillsetDisplay;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private PublicationStatus status;
	
	@Column(name = "effective")
	private Date effective;

	@Column(name = "jurisdiction")
	private String jurisdiction;

	public Long getId() {
		return id;
	}

	public String getScenarioId() {
		return scenarioId;
	}

	public String getDescription() {
		return description;
	}

	public String getPurpose() {
		return purpose;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public String getPartyDisplay() {
		return partyDisplay;
	}

	public String getSkillsetCode() {
		return skillsetCode;
	}

	public String getSkillsetDisplay() {
		return skillsetDisplay;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public void setPartyDisplay(String partyDisplay) {
		this.partyDisplay = partyDisplay;
	}

	public void setSkillsetCode(String skillsetCode) {
		this.skillsetCode = skillsetCode;
	}

	public void setSkillsetDisplay(String skillsetDisplay) {
		this.skillsetDisplay = skillsetDisplay;
	}

	public PublicationStatus getStatus() {
		return status;
	}

	public void setStatus(PublicationStatus status) {
		this.status = status;
	}

	public Date getEffective() {
		return effective;
	}

	public void setEffective(Date effective) {
		this.effective = effective;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
}
