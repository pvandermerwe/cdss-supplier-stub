package uk.nhs.cdss.repos;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import uk.nhs.cdss.entities.ServiceDefinitionEntity;

@Repository
public interface ServiceDefinitionRepository extends JpaRepository<ServiceDefinitionEntity, Long> {

	@Query("SELECT sd " +
		   "FROM ServiceDefinitionEntity sd, DataRequirementEntity dr " +
		   "JOIN dr.codedData cd " +
		   "WHERE sd.id = dr.serviceDefinitionId " +
		   "AND sd.status = :status " +
		   "AND sd.effective >= :effective " +
		   "AND sd.jurisdiction = :jurisdiction " +
		   "AND sd.partyCode = :partyCode " +
		   "AND sd.skillsetCode = :skillsetCode " +
		   "AND dr.questionnaireId = :questionnaireId " +
		   "AND cd.id = :observationId ")
	public List<ServiceDefinitionEntity> search(
			@Param("status") PublicationStatus status, @Param("effective") Date effective, 
			@Param("jurisdiction") String jurisdiction, @Param("observationId") Long observationId, 
			@Param("questionnaireId") Long questionnaireId, @Param("partyCode") String partyCode,
			@Param("skillsetCode") String skillsetCode);
	
}
