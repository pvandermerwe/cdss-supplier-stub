package uk.nhs.cdss.resourceProviders;

import java.util.List;

import org.hl7.fhir.dstu3.model.Enumerations.PublicationStatus;
import org.hl7.fhir.dstu3.model.GuidanceResponse;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.ServiceDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import uk.nhs.cdss.entities.ServiceDefinitionEntity;
import uk.nhs.cdss.repos.ServiceDefinitionRepository;
import uk.nhs.cdss.resourceBuilder.ServiceDefinitionBuilder;
import uk.nhs.cdss.services.EvaluateService;

@Component
public class ServiceDefinitionProvider implements IResourceProvider {
	private static final String EVALUATE = "$evaluate";

	@Autowired
	private EvaluateService evaluateService;

	@Autowired
	private ServiceDefinitionBuilder serviceDefinitionBuilder;
	
	@Autowired 
	private ServiceDefinitionRepository serviceDefinitionRepository;

	@Override
	public Class<ServiceDefinition> getResourceType() {
		return ServiceDefinition.class;
	}

	@Operation(name = EVALUATE, idempotent = true)
	public GuidanceResponse evaluate(@IdParam IdType serviceDefinitionId, @ResourceParam Parameters params) {
		return evaluateService.getGuidanceResponse(params, serviceDefinitionId.getIdPartAsLong());
	}

	@Read
	public ServiceDefinition getServiceDefinitionById(@IdParam IdType serviceDefinitionId) {
		return serviceDefinitionBuilder.createServiceDefinition(serviceDefinitionId.getIdPartAsLong());
	}
	
	@Search
	public ServiceDefinition findServiceDefinitions(
			@RequiredParam(name=ServiceDefinition.SP_STATUS) StringParam status,
			@RequiredParam(name=ServiceDefinition.SP_EFFECTIVE) DateParam effective,
			@RequiredParam(name=ServiceDefinition.SP_JURISDICTION) TokenParam jurisdiction,
			@RequiredParam(name="trigger") StringParam trigger,
			@RequiredParam(name="partyCode") StringParam partyCode,
			@RequiredParam(name="skillsetCode") StringParam skillsetCode) {
							
		List<ServiceDefinitionEntity> entities = serviceDefinitionRepository.search(
				PublicationStatus.valueOf(status.getValue()), effective.getValue(), jurisdiction.getValue(), 
				Long.valueOf(trigger.getValue().split(":")[1]), Long.valueOf(trigger.getValue().split(":")[0]),
				partyCode.getValue(), skillsetCode.getValue());
		
		return serviceDefinitionBuilder.createServiceDefinition(entities.get(0), true);	
	}

}
